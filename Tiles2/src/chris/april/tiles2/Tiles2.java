package chris.april.tiles2;

import chris.april.tiles.screens.Game;
import chris.april.tiles.screens.Screen;
import chris.april.tiles.screens.StartupScreen;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Tiles2 extends Game {
	protected Screen getStartScreen() {
		return new StartupScreen(this);
	}
}