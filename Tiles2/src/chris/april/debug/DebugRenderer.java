package chris.april.debug;

import chris.april.tiles.rendering.UiModel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

public class DebugRenderer implements Disposable {

	/** the shot mesh **/
	protected BitmapFont font;

	/** sprite batch to draw text **/
	protected SpriteBatch spriteBatch;

	protected DebugUiModel debugUiModel;

	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;


	private UiModel uiModel;


	public DebugRenderer(OrthographicCamera camera, DebugUiModel debugUiModel, UiModel uiModel) {

	this.debugUiModel = debugUiModel;
		
		this.uiModel = uiModel;

		this.camera = camera;
		
		this.shapeRenderer = new ShapeRenderer();

		this.spriteBatch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("data/font10.fnt"),
				Gdx.files.internal("data/font10.png"), false);
	}

	public void render() {

		this.shapeRenderer.setProjectionMatrix(camera.combined);
		
		this.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

		this.shapeRenderer.setColor(Color.PINK);
		if (this.debugUiModel.touchDownPosition != null)
			this.shapeRenderer.line(0, 0, this.debugUiModel.touchDownPosition.x,
					this.debugUiModel.touchDownPosition.y);

		this.shapeRenderer.setColor(Color.MAGENTA);
		if (this.debugUiModel.panPosition != null)
			this.shapeRenderer.line(0, 0, this.debugUiModel.panPosition.x,
					this.debugUiModel.panPosition.y);

		Vector2 subV = null;
		if (this.debugUiModel.touchDownPosition != null
				&& this.debugUiModel.panPosition != null) {

			subV = this.debugUiModel.panPosition.cpy().sub(
					this.debugUiModel.touchDownPosition);

			this.shapeRenderer.line(this.debugUiModel.touchDownPosition.x,
					this.debugUiModel.touchDownPosition.y,
					this.debugUiModel.touchDownPosition.cpy().add(subV).x,
					this.debugUiModel.touchDownPosition.cpy().add(subV).y);
		}
		this.shapeRenderer.end();

		

		this.spriteBatch.begin();
		if (subV != null) {
			// Substraktion von camera.viewportHeight wegen
			// Camera.setToOrtho(true, ....
			this.font.draw(spriteBatch, "angle: " + subV.angle(),
					this.debugUiModel.touchDownPosition.x,
					this.camera.viewportHeight
							- this.debugUiModel.touchDownPosition.y);

			this.font.draw(spriteBatch, "length: " + subV.len(),
					this.debugUiModel.touchDownPosition.x + 10,
					this.camera.viewportHeight
							- this.debugUiModel.touchDownPosition.y + 10);
		}
		
		
		this.font.draw(spriteBatch, "Tile: " + this.debugUiModel.touchDownPosition,
				0 ,
				40);		
		
		this.spriteBatch.end();
	}

	public void dispose() {
		this.spriteBatch.dispose();
		this.font.dispose();
		this.shapeRenderer.dispose();
	}

	public Camera getCamera() {
		return this.camera;
	}

	public void renderFPS() {
		this.spriteBatch.begin();
		this.font.draw(spriteBatch,
				"fps: " + Gdx.graphics.getFramesPerSecond(), 0, 20);
		this.spriteBatch.end();
	}

	public void resize(float width, float height) {
		if (this.camera instanceof OrthographicCamera)
			((OrthographicCamera) this.camera).setToOrtho(true, width, height);
	}

}
