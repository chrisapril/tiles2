package chris.april.debug;

import chris.april.tiles.controller.Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * 
 * @author chrisapril
 * 
 */

public class DebugSelectionController extends GestureDetector.GestureAdapter // GestureDetector.GestureAdapter
		implements Controller {

	private final DebugUiModel debugUiModel;
	private Camera camera;
	
	private Vector3 pos;

	public DebugSelectionController(DebugUiModel debugUiModel, Camera camera) {
		this.debugUiModel = debugUiModel;
		this.camera = camera;
		
		this.pos = new Vector3();
	}

	@Override
	public void update(float deltaTime) {
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		Gdx.app.debug(this.getClass().getName(), "tap");

		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		Gdx.app.debug(this.getClass().getName(), "touchDown");

		
		camera.unproject(pos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		
		
		this.debugUiModel.panPosition = null;

		this.debugUiModel.touchDownPosition = new Vector2(pos.x, pos.y);

		Gdx.app.debug(this.getClass().getName(), "touchDown, touchDownPosition: "
				+ this.debugUiModel.touchDownPosition);
		return super.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean longPress(float x, float y) {
		return super.longPress(x, y);
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		Gdx.app.debug(this.getClass().getName(), "fling velocityX: " + velocityX
				+ ", velocityY: " + velocityY);

		return super.fling(velocityX, velocityY, button);
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		Gdx.app.debug(this.getClass().getName(), "pan x:" + x + ", y:" + y
				+ ", deltaX: " + deltaX + ", deltaY:" + deltaY);

		camera.unproject(pos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		
		
		this.debugUiModel.panPosition = new Vector2(pos.x, pos.y);

		Gdx.app.debug(this.getClass().getName(), "pan panPosition: "
				+ this.debugUiModel.panPosition);
		return super.pan(x, y, deltaX, deltaY);
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return super.zoom(initialDistance, distance);
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return super
				.pinch(initialPointer1, initialPointer2, pointer1, pointer2);
	}


}