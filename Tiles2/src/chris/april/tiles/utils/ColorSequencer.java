package chris.april.tiles.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;


/** this class generates a sequence of contrasting colors - useful for
    highlighting different parts of a diagram with different colors
 */
public class ColorSequencer {

  public ColorSequencer() {
		super();
//		this.curColor =  new Color(130,251,23, 1);
		
		this.curColor =  new Color(130f/256f,251f/256f,23f/256f, 256f/256F);
	
  
  }

/** returns the current color in the sequence */
  public Color current() { return curColor; }
  
  /** change the color to a contrasting color */

 public Color next() {
    
	float r = curColor.r;
    if (r == 0) r = 1;
    
    float g = curColor.g;
    if (g == 0) g = 1;
    
    float b = curColor.b;
    if (b == 0) b = 1;
    
    // System.out.println("Old color: r=" + r + " g=" + g + " b=" + b);
    float newr = (MathUtils.random() * 43 * (b*256)) % 256;
    newr = newr / 256;
    float newg = r;
    float newb = g;
    if (newr < 100/256 && newg < 100/256 && newb < 100/256) {
      if (MathUtils.random() < 0.5) newg += 100f/256f;
      else newb += 100f/256f;
    }
    // System.out.println("New color: r=" + newr + " g=" + newg + " b=" + newb);
    return (curColor = new Color(newr,newg,newb,1));
  }

  private Color curColor;
}