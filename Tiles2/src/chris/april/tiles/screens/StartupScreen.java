package chris.april.tiles.screens;

import chris.april.tiles.test.Test;



public class StartupScreen extends Screen {

	public StartupScreen(Game game) {
		super(game);
		Resources.load();
	}

	public void pause() {
	}

	public void resume() {
	}

	public void render() {
			this.game.setScreen(new Test(this.game));
	}

	public void dispose() {
	}

	public void resize(int width, int height) {
	}
}
