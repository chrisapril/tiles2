package chris.april.tiles.screens;

import chris.april.tiles.controller.ControllerManager;


public abstract class Screen {

	protected final Game game;
	protected final ControllerManager controllerManager;

	public Screen(Game game) {
		this.game = game;
		this.controllerManager = new ControllerManager(game.getMultiplexer());
	}

	public abstract void pause();

	public abstract void resume();

	public abstract void render();

	public abstract void dispose();

	public abstract void resize(int paramInt1, int paramInt2);

	public ControllerManager getControllerManager() {
		return this.controllerManager;
	}

}
