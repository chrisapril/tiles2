 package chris.april.tiles.screens;
 
 import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
 
 public abstract class Game
   implements ApplicationListener
 {
   private Screen currentScreen;
   private SpriteBatch batch;
   private InputMultiplexer multiplexer;
 
   protected abstract Screen getStartScreen();
 
   public void create()
   {
     this.batch = new SpriteBatch();
     this.multiplexer = new InputMultiplexer();
     Gdx.input.setInputProcessor(this.multiplexer);
     this.currentScreen = getStartScreen();
   }
 
   public void resize(int width, int height)
   {
     this.currentScreen.resize(width, height);
   }
 
   public void render()
   {
     Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
     Gdx.gl.glClear(16640);
     this.currentScreen.getControllerManager().update(Gdx.graphics.getDeltaTime());
     this.currentScreen.render();
   }
 
   public void pause()
   {
     this.currentScreen.pause();
   }
 
   public void resume()
   {
     this.currentScreen.resume();
   }
 
   public void dispose()
   {
     this.currentScreen.getControllerManager().dispose();
     this.currentScreen.dispose();
   }
 
   public void setScreen(Screen newScreen) {
     this.currentScreen.getControllerManager().dispose();
     this.currentScreen.dispose();
     this.currentScreen = newScreen;
   }
 
   public SpriteBatch getBatch() {
     return this.batch;
   }
 
   public InputMultiplexer getMultiplexer() {
     return this.multiplexer;
   }
 }

/* Location:           /Users/chrisapril/Dropbox/dungeon_architect/tiny dungeons/tinydungeons-1-1/
 * Qualified Name:    com.april.tinydungeontribes.screens.Game
 * JD-Core Version:    0.6.2
 */