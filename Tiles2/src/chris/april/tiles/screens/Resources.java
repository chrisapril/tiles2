package chris.april.tiles.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Resources {
	public static BitmapFont font;
	public static Skin skin;
	private static String[] uiImages = { "ui/build-btn.png" };

	public static void load() {

		// http://www.badlogicgames.com/wordpress/?p=2300
//		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(
//				Gdx.files.internal("ui/font.ttf"));
//		font = gen.generateFont(16);
//		gen.dispose();
//
//		createSkin();
	}

	private static void createSkin() {
		TextureAtlas atlas = createAtlas();
		skin = new Skin();
		skin.add("default", new Label.LabelStyle(font, Color.WHITE));

		skin.add(
				"build-btn",
				new Button.ButtonStyle(new TextureRegionDrawable(atlas
						.findRegion("ui/build-btn.png")),
						new TextureRegionDrawable(atlas
								.findRegion("ui/build-btn.png")),
						new TextureRegionDrawable(atlas
								.findRegion("ui/build-btn.png"))));

	}

	private static TextureAtlas createAtlas() {
		PixmapPacker packer = new PixmapPacker(1024, 1024,
				Pixmap.Format.RGBA8888, 2, false);
		for (String image : uiImages) {
			Pixmap pixmap = new Pixmap(Gdx.files.internal(image));
			packer.pack(image, pixmap);
		}
		return packer.generateTextureAtlas(TextureFilter.Nearest,
				TextureFilter.Nearest, false);
	}
}
