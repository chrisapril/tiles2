package chris.april.tiles.screens;

import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class StageScreen extends Screen {
	protected Stage stage;

	public StageScreen(Game game) {
		super(game);
		this.stage = new Stage(480.0F, 320.0F, false, game.getBatch());
		game.getMultiplexer().addProcessor(this.stage);
	}

	public void dispose() {
		this.game.getMultiplexer().removeProcessor(this.stage);
		this.stage.dispose();
	}

}
