 package chris.april.tiles.controller;
 
 import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IdentityMap;
 
 public class ControllerManager
 {
   private final InputMultiplexer multiplexer;
   private final Array<Controller> controllers;
   private final IdentityMap<Controller, GestureDetector> detectors;
   private final Array<EventListener> eventListener;
 
   public ControllerManager(InputMultiplexer multiplexer)
   {
     this.multiplexer = multiplexer;
     this.controllers = new Array<Controller>();
     this.detectors = new IdentityMap<Controller, GestureDetector>();
     
     this.eventListener = new Array<EventListener>();
   }
 
   public void addController(Controller controller) {
     this.controllers.add(controller);
     if ((controller instanceof InputProcessor)) {
       this.multiplexer.addProcessor((InputProcessor)controller);
     } 
     
     if ((controller instanceof GestureDetector.GestureListener)) {
       GestureDetector detector = new GestureDetector((GestureDetector.GestureListener)controller);
       this.detectors.put(controller, detector);
       this.multiplexer.addProcessor(detector);     
       if ((controller instanceof GestureController)) {
    	   ((GestureController) controller).setGestureDetector(detector);
    	   
       }
       
     }
     //pasted by may#
     if (controller instanceof EventListener) {
    	 this.eventListener.add((EventListener)controller);
       }
   }
 
   public void removeController(Controller controller) {
     this.controllers.removeValue(controller, true);
     if ((controller instanceof InputProcessor)) {
       this.multiplexer.removeProcessor((InputProcessor)controller);
     } else if ((controller instanceof GestureDetector.GestureListener)) {
       GestureDetector detector = (GestureDetector)this.detectors.get(controller);
       this.detectors.remove(controller);
       this.multiplexer.removeProcessor(detector);
     }
     
     if (controller instanceof EventListener) {
    	 this.eventListener.removeValue((EventListener)controller,true);
     }
   }
 
   public void update(float deltaTime) {
     for (Controller controller : this.controllers)
       controller.update(deltaTime);
   }
 
   public void dispose()
   {
     for (Controller controller : this.controllers)
       if ((controller instanceof InputProcessor)) {
         this.multiplexer.removeProcessor((InputProcessor)controller);
       } else if ((controller instanceof GestureDetector.GestureListener)) {
         GestureDetector detector = (GestureDetector)this.detectors.get(controller);
         this.detectors.remove(controller);
         this.multiplexer.removeProcessor(detector);
       }else if (controller instanceof ChangeListener) {
      	 this.eventListener.removeValue((ChangeListener)controller,true);
       }
   }
 
   public InputMultiplexer getInputMultiplexer()
   {
     return this.multiplexer;
   }

public Array<EventListener> getEventListener() {
	return eventListener;
}
   
   
 }
