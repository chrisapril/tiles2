package chris.april.tiles.controller;

import com.badlogic.gdx.input.GestureDetector;

public abstract interface Controller
{
  public abstract void update(float paramFloat);
  
}
