package chris.april.tiles.controller;

import chris.april.tiles.model.Level;
import chris.april.tiles.model.LevelObject;
import chris.april.tiles.model.SteerableObject;
import chris.april.tiles.rendering.UiModel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;

/**
 * 
 * @author chrisapril
 * 
 */

public class SelectionController extends GestureDetector.GestureAdapter // GestureDetector.GestureAdapter
		implements GestureController {

	private final UiModel uiModel;
	private Level level;
	private Camera camera;

	private final Vector3 pos;

	private final Array<LevelObject> selectedObjects;

	public SelectionController(Level level, UiModel uiModel, Camera camera) {
		this.level = level;
		this.uiModel = uiModel;
		this.camera = camera;

		this.offsetVector = new Vector2();
		this.pos = new Vector3();
		this.selectedObjects = new Array<LevelObject>();

	}

	@Override
	public void update(float deltaTime) {
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		// the unproject method takes a Vector3 in window coordinates (origin in
		// upper left corner, y-axis pointing down) and transforms it to world
		// coordinates.

		this.pos.set(x, y, 0);
		camera.unproject(this.pos);
		
		for (LevelObject obj : this.level.getLevelObjects()) {

//			obj.getPos().set(this.pos.x-(obj.getWidth()*.5f), this.pos.y-(obj.getHeight()*.5f));
			if(obj instanceof SteerableObject)
				((SteerableObject) obj).setTarget(this.pos.x, this.pos.y);

		}

		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		this.selectedObjects.clear();
		beginTouch = true;

		this.pos.set(x, y, 0);
		camera.unproject(this.pos);

		for (LevelObject obj : this.level.getLevelObjects()) {
			
			Gdx.app.debug(this.getClass().getName(), "touchDown, obj.x: "+obj.getPos().x +", obj.y: "+ obj.getPos().y);
			
			Rectangle re = new Rectangle(obj.getPos().x, obj.getPos().y,
					obj.getWidth(), obj.getHeight());

			if (re.contains(this.pos.x, this.pos.y)) {
				this.selectedObjects.add(obj);
			}
		}
		Gdx.app.debug(this.getClass().getName(), "touchDown, num selectedObjects: "+this.selectedObjects.size);

		
		return super.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return super.longPress(x, y);
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		Gdx.app.debug(this.getClass().getName(), "fling velocityX: "
				+ velocityX + ", velocityY: " + velocityY);
		return super.fling(velocityX, velocityY, button);
	}

	boolean beginTouch;

	Vector2 offsetVector;
	boolean pan;
	private GestureDetector detector;
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		Gdx.app.debug(this.getClass().getName(), "pan x:" + x + ", y:" + y
				+ ", deltaX: " + deltaX + ", deltaY:" + deltaY+", num selectedObj: "+this.selectedObjects.size);

		pan = true;
		
		this.pos.set(x, y, 0);
		camera.unproject(this.pos);
		
		for (LevelObject obj : this.selectedObjects) {
			Gdx.app.debug(this.getClass().getName(), "pan offsetVector:" +offsetVector+", len: "+offsetVector.len());
			obj.getPos().add(deltaX, -1 * deltaY);
			
			if(obj instanceof SteerableObject)
				((SteerableObject) obj).setTarget(this.pos.x, this.pos.y);
		}		
		
		offsetVector.set(0,0);
		this.beginTouch = false;
		
		return super.pan(x, y, deltaX, deltaY);
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return super.zoom(initialDistance, distance);
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return super
				.pinch(initialPointer1, initialPointer2, pointer1, pointer2);
	}

	@Override
	public void setGestureDetector(GestureDetector detector) {
		this.detector = detector;
		

		this.detector.setTapSquareSize(4);
		
	}

}