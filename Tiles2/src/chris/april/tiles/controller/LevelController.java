package chris.april.tiles.controller;

import chris.april.tiles.model.Level;
import chris.april.tiles.model.LevelObject;
import chris.april.tiles.model.SteerableObject;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class LevelController implements Controller{
	private final Level lvl;
	private float accumulator;
	private static final float TICK = 0.01666667F;
	
	
	public LevelController(Level lvl) {
		super();
		this.lvl = lvl;
		tmp = new Vector2();
	}
	
	Vector2 tmp;

	@Override
	public void update(float deltaTime) {

		deltaTime = MathUtils.clamp(deltaTime, 0, 0.030f);
		accumulator += deltaTime;
		
		while (accumulator > TICK) {
			accumulator -= TICK;
			for (LevelObject object : lvl.getLevelObjects()) {
				if(object instanceof SteerableObject){
					
					((SteerableObject) object).getAcceleration().mul(0);
					if(tmp.set(object.getPos().cpy().add(object.getWidth()/2, object.getHeight()/2))
							.sub(((SteerableObject) object).getTarget()).len() > object.getWidth()){
						((SteerableObject) object).seek();	
					}else{
						((SteerableObject) object).getVelocity().mul(0);
					}
					((SteerableObject) object).integrate(TICK);
				}
			}
		}
	}
	
	
	
}
