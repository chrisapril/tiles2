package chris.april.tiles.controller;

import com.badlogic.gdx.input.GestureDetector;

public abstract interface GestureController extends Controller
{
  public void setGestureDetector(GestureDetector detector);
  
  
}
