package chris.april.tiles.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class SteerableObject extends LevelObject {

	private State state;
	private float stateTime;
	private final Vector2 acceleration;
	private final Vector2 velocity;
	private final Vector2 direction;
	private Behaviour behaviour;
	private Vector2 target;
	private float speed;

	public SteerableObject(Vector2 pos, String graphic, String name,
			float width, float height, 
			 Behaviour behaviour, float speed) {
		
		super(pos, graphic, name, width, height);
		
		this.speed = speed;
		this.state = state.Idle;
		this.stateTime = 0;
		
		this.acceleration = new Vector2();
		this.velocity = new Vector2();
		this.direction = new Vector2();
		this.behaviour = behaviour;
		this.target = new Vector2();
	}

	public State getState() {
		return this.state;
	}

	public float getStateTime() {
		return this.stateTime;
	}

	public void setState(State state) {
		this.state = state;
		this.stateTime = 0.0F;
		this.velocity.set(0.0F, 0.0F);
	}

	public void setStateTime(float stateTime) {
		this.stateTime = stateTime;
	}

	public Vector2 getAcceleration() {
		return this.acceleration;
	}

	public Vector2 getVelocity() {
		return this.velocity;
	}

	public Vector2 getDirection() {
		return this.direction;
	}

	public void updateStateTime(float deltaTime) {
		this.stateTime += deltaTime;
	}

	public Behaviour getBehaviour() {
		return this.behaviour;
	}

	public void setBehaviour(Behaviour behaviour) {
		this.behaviour = behaviour;
	}

	public Vector2 getTarget() {
		return this.target;
	}

	public void setTarget(float x, float y) {
		this.target.set(x, y);
	}



	public static enum State {
		Idle, Move, Attack, Die;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void integrate(float deltaTime) {
		this.velocity.x += deltaTime * this.acceleration.x;
		this.velocity.y += deltaTime * this.acceleration.y;
		if (this.velocity.len2() > this.getSpeed()
				* this.getSpeed()) {
			this.velocity.nor().mul(this.getSpeed());
		}
		super.getPos().x += deltaTime * this.velocity.x;
		super.getPos().y += deltaTime * this.velocity.y;
		if (this.velocity.len2() != 0.0F)
			this.direction.set(this.velocity).nor();
	}

	Vector2 tmp = new Vector2();

	public void seek() {
		this.tmp.set(target).sub(
				super.getPos());
		this.getAcceleration().add(this.tmp.mul(5.0F));
	}

}
