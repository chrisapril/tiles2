package chris.april.tiles.model;


import com.badlogic.gdx.utils.Array;

public class Level {
	
	private final Array<LevelObject> renderableObjects;
	
	public Level(Array<LevelObject> renderableObjects) {
		super();
		this.renderableObjects = renderableObjects;
	}
	
	public Array<LevelObject> getLevelObjects() {
		return renderableObjects;
	}
	
}
