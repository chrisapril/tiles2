package chris.april.tiles.model;

import com.badlogic.gdx.math.Vector2;

public class LevelObject {

	private final Vector2 pos;
	private final String graphic;
	private final String name;
	
	private float width, height;
	
	
	public LevelObject(Vector2 pos, String graphic, String name, float width, float height) {
		super();
		this.pos = pos;
		this.graphic = graphic;
		this.name = name;
		this.width = width;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public String getGraphic() {
		return graphic;
	}

	public Vector2 getPos() {
		return pos;
	}

	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}


	
	
}
