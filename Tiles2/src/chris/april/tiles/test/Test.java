package chris.april.tiles.test;

import chris.april.debug.DebugRenderer;
import chris.april.debug.DebugSelectionController;
import chris.april.debug.DebugUiModel;
import chris.april.tiles.controller.LevelController;
import chris.april.tiles.controller.SelectionController;
import chris.april.tiles.model.Behaviour;
import chris.april.tiles.model.Level;
import chris.april.tiles.model.LevelObject;
import chris.april.tiles.model.SteerableObject;
import chris.april.tiles.rendering.BackgroundRenderer;
import chris.april.tiles.rendering.Renderer;
import chris.april.tiles.rendering.SpritesPack;
import chris.april.tiles.rendering.UiModel;
import chris.april.tiles.screens.Game;
import chris.april.tiles.screens.Screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Test extends Screen {

	private UiModel uiModel;

	private Renderer renderer;

	private OrthographicCamera camera;

	private DebugUiModel debugUiModel;
	private DebugRenderer debugRenderer;
	private SelectionController selectionController;

	private SpritesPack spritesPack;

	private Level lvl;

	private BackgroundRenderer backgroundRenderer;

	public Test(Game game) {

		super(game);

		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		createCamera();

		Array<LevelObject> objects = new Array<LevelObject>();

		LevelObject o = new LevelObject(new Vector2(60, 60),
				"data/Blocks_01/Blocks_01_256x256_Alt_00_001.png", "block_01",
				20f, 20f);
		objects.add(o);

		o = new SteerableObject(new Vector2(60, 60),
				"data/Blocks_01/Blocks_01_256x256_Alt_00_001.png", "block_01",
				20f, 20f, new Behaviour(), 38f);
		objects.add(o);
		
		this.lvl = new Level(objects);

		this.spritesPack = new SpritesPack(lvl);

		this.uiModel = new UiModel();
		this.renderer = new Renderer(this.camera);
		
		this.backgroundRenderer = new BackgroundRenderer(this.camera);

		
		this.controllerManager.addController(new SelectionController(lvl, uiModel, camera));
		this.controllerManager.addController(new LevelController(lvl));
		
		
		/** DEBUG **/
		this.debugUiModel = new DebugUiModel();
		this.debugRenderer = new DebugRenderer(camera, debugUiModel, uiModel);
		this.controllerManager.addController(new DebugSelectionController(
				debugUiModel, this.camera));

		
		
		
	}

	/**
	 * http://code.google.com/p/libgdx/wiki/ProjectionViewportCamera
	 * 
	 * s. MoveSpriteExample.java, wenn Ortho = false dann muss man die Screen
	 * Koordinaten auf die Weltkoordinaten mappen. (das wird im
	 * SelectionController gemacht)
	 */
	private void createCamera() {
		// this.camera = new OrthographicCamera();
		// this.camera.setToOrtho(false, Gdx.graphics.getWidth(),
		// Gdx.graphics.getHeight());

		// create an {@link OrthographicCamera} which is used to transform
		// touch coordinates to world coordinates.
		camera = new OrthographicCamera();

		// we want the camera to setup a viewport with pixels as units, with the
		// y-axis pointing upwards. The origin will be in the lower left corner
		// of the screen.
		camera.setToOrtho(false);

	}

	/**
	 * s. http://code.google.com/p/libgdx/wiki/ProjectionViewportCamera
	 */
	public void render() {
		camera.update();
		camera.apply(Gdx.gl10);

		// set the clear color and clear the screen.
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		/** Background **/
		this.backgroundRenderer.render(this.spritesPack);

		this.spritesPack.update();
		this.renderer.render(this.spritesPack);
		
		/** DEBUG **/
		this.debugRenderer.renderFPS();
		this.debugRenderer.render();
		// this.debugRenderer.renderTileRectangle();

	}

	public void pause() {
		System.out.println("pause");
	}

	public void resume() {
	}

	public void dispose() {
		this.spritesPack.dispose();
		this.renderer.dispose();
	}

	/** FIXME: nach resize funktioniert SelectionController nicht mehr */
	public void resize(int width, int height) {
		// createCamera();
	}
}
