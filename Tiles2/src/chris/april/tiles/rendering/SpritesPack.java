package chris.april.tiles.rendering;

import chris.april.tiles.model.Level;
import chris.april.tiles.model.LevelObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public class SpritesPack implements Disposable {
	private TextureAtlas atlas;
	private Array<SpritePack> sprites;
	
	
	public SpritesPack(Level lvl) {
		
		this.sprites = new Array<SpritePack>();
		
		PixmapPacker packer = new PixmapPacker(1024, 1024,
				Pixmap.Format.RGBA8888, 2, false);
	
		for (LevelObject object : lvl.getLevelObjects()) {

			if (packer.getRect(object.getGraphic()) == null)
				packer.pack(object.getGraphic(), new Pixmap(Gdx.files.internal(object.getGraphic())));
		}
		
		this.atlas = packer.generateTextureAtlas(TextureFilter.Nearest,
				TextureFilter.Nearest, false);

		this.sprites.addAll(createSprites(this.atlas, lvl.getLevelObjects()));
	}
	
	
	private Array<SpritePack> createSprites(TextureAtlas atlas,
			Array<LevelObject> objects) {
		
		// this.decals = new Array();
		Array<SpritePack> sprites = new Array<SpritePack>();

		for (LevelObject object : objects) {
			TextureRegion frame = atlas.findRegion(object.getGraphic());
			
			Sprite sprite = new Sprite(frame);
			
			sprite.setSize(object.getWidth(), object.getHeight());
			sprite.setPosition(object.getPos().x, object.getPos().y);

			
			SpritePack pack = new SpritePack(
				 object, sprite);
			sprites.add(pack);
		}
		return sprites;
	}

	public void dispose() {
		this.atlas.dispose();
	}

	public Array<SpritePack> getSprites() {
		return this.sprites;
	}

	
	public void update() {
		for (SpritePack spritePack : this.sprites) {
			spritePack.update();
		}
	}

	
	/**
	 * Kapselt ein Sprite so da� wird eine Trennung zwischen Model und View erreicht wird.
	 * @author Christian.May
	 *
	 */
	public static class SpritePack {
		private final LevelObject object;
		private Sprite sprite;
		

		public SpritePack(LevelObject object, Sprite sprite) {
			this.object = object;
			this.sprite = sprite;
			
		}

		public Sprite getSprite() {
			return this.sprite;
		}


		public LevelObject getObject() {
			return this.object;
		}

		/**
		 * Updated die Position des Sprites
		 */
		public void update() {
			this.sprite.setPosition(this.object.getPos().x, this.object.getPos().y);
		}
	}

}

