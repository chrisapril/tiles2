package chris.april.tiles.rendering;

import chris.april.tiles.model.Level;
import chris.april.tiles.rendering.SpritesPack.SpritePack;
import chris.april.tiles.utils.ColorSequencer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

/**
 * nach SpriteBatchRotationTest.java
 * 
 * @author Christian.May
 * 
 */
public class BackgroundRenderer implements Disposable {

	/** wei�e textur zum tiles zeichnen **/
	protected Texture tileTexture;


	Texture[] textures;
	public static float fieldS = 1f;
	public static float tileScale = 0.95f;

	/** sprite batch to draw tiles **/
	protected SpriteBatch spriteBatch;

	protected Camera camera;

	private Texture background;


	private ColorSequencer colorSequencer;

	// Padding zwischen den Tiles auf dem Feld
	public static final float tilePadding = 4f;

	// Padding innerhalb des Tiles zwischen Background und Tile
	public static final float innerPadding = 4f;

	public Camera getCamera() {
		return this.camera;
	}

	public void resize(float width, float height) {
		if (this.camera instanceof OrthographicCamera)
			((OrthographicCamera) this.camera).setToOrtho(true, width, height);
	}

	int num = 10;


	private Color[][] field;
	
	public BackgroundRenderer(Camera camera) {
		this.camera = camera;
		this.spriteBatch = new SpriteBatch();
		this.background = new Texture(Gdx.files.internal("data/tile_rec.png"));
		
		this.colorSequencer = new ColorSequencer();		
		
		this.field = new Color[num][num];
		
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				Color c =this.colorSequencer.next();		
				
				field[i][j] = new Color(c.r, c.g, c.b, 1f);//.01f);
			}
		}
	}

	public void render(SpritesPack spritesPack) {
		
		
		spriteBatch.enableBlending();
		spriteBatch.begin();
		
		int w = Gdx.graphics.getWidth()/num;
		int h = Gdx.graphics.getHeight() /num;
		
		
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				spriteBatch.setColor(field[i][j]);
				spriteBatch.draw(background, i*w, j*h, w,
						h);
			}
		}
		
		spriteBatch.end();

	}

	
	public void dispose() {
		this.spriteBatch.dispose();
	}

}
