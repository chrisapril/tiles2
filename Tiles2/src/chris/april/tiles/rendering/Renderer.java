package chris.april.tiles.rendering;

import chris.april.tiles.model.Level;
import chris.april.tiles.rendering.SpritesPack.SpritePack;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

/**
 * nach SpriteBatchRotationTest.java
 * 
 * @author Christian.May
 * 
 */
public class Renderer implements Disposable {

	/** wei�e textur zum tiles zeichnen **/
	protected Texture tileTexture;

	Texture[] textures;
	public static float fieldS = 1f;
	public static float tileScale = 0.95f;

	/** sprite batch to draw tiles **/
	protected SpriteBatch spriteBatch;

	protected Camera camera;

	private Texture background;

	// Padding zwischen den Tiles auf dem Feld
	public static final float tilePadding = 4f;

	// Padding innerhalb des Tiles zwischen Background und Tile
	public static final float innerPadding = 4f;

	public Camera getCamera() {
		return this.camera;
	}

	public void resize(float width, float height) {
		if (this.camera instanceof OrthographicCamera)
			((OrthographicCamera) this.camera).setToOrtho(true, width, height);
	}

	public Renderer(Camera camera) {

		this.camera = camera;

		this.spriteBatch = new SpriteBatch();

		textures = new Texture[6];

		for (int x = 0; x < 6; x++) {

			textures[x] = new Texture(
					Gdx.files
							.internal("data/Blocks_01/Blocks_01_256x256_Alt_00_00"
									+ (x + 1) + ".png"));

		}

		this.background = new Texture(Gdx.files.internal("data/clouds.png"));

	}

	public void render(SpritesPack spritesPack) {
//		Gdx.graphics.getGL10().glClear(GL10.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();
//		spriteBatch.draw(background, 0, 0, Gdx.graphics.getWidth(),
//				Gdx.graphics.getHeight());

		for (SpritePack spritePack : spritesPack.getSprites()) {
			spritePack.getSprite().draw(spriteBatch);
		}
		spriteBatch.end();

//		Gdx.graphics.getGL10().glFlush();
	}

	
	public void dispose() {
		this.spriteBatch.dispose();
	}

}
